package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import java.awt.Component;

import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;

import com.alee.laf.WebLookAndFeel;

import dbmodel.Contract;
import dbmodel.Option;

import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.JMenuBar;
import javax.swing.Box;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.awt.SystemColor;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JFormattedTextField;

@SuppressWarnings("serial")
public class MainContractWindow extends JFrame {

	private JPanel contentPane;
	private JTable contractsTable;
	private JTable optionsTable;
	private JMenuItem saveContractMenuItem;
	private JTextField brandTextField;
	private JTextField modelTextField;
	private JTextField priceTextField;
	private JTextField prodYearTextField;
	private JFormattedTextField mileageTextField;
	private JTextField contractPriceTextField;
	private JFormattedTextField contractStartDateTextField;
	private JFormattedTextField contractEndDateTextField;
	private JTextField contractMileageTextField;
	private JMenuItem addingOptionMenuItem;
	private JMenuItem newContractMenuItem;
	private JMenuItem editOptionsMenuItem;

	private JButton deleteContractButton;
	private JButton loadContractButton;
	private JButton addOptionButton;
	private JButton deleteOptionButton;

	private JLabel sumarizedLabel;
	private JLabel sumarizedPriceLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("unused")
			public void run() {
				try {
					UIManager.setLookAndFeel(WebLookAndFeel.class.getCanonicalName());
					MainContractWindow frame = new MainContractWindow();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws ParseException
	 */
	public MainContractWindow() throws ParseException {
		setAutoRequestFocus(false);

		setSize(1004, 600);
		setLocationRelativeTo(null);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Contract");
		menuBar.add(mnNewMenu);

		newContractMenuItem = new JMenuItem("New Contract");
		mnNewMenu.add(newContractMenuItem);

		saveContractMenuItem = new JMenuItem("Save Contract");
		mnNewMenu.add(saveContractMenuItem);

		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);

		addingOptionMenuItem = new JMenuItem("Add Option...");
		mnOptions.add(addingOptionMenuItem);

		editOptionsMenuItem = new JMenuItem("Edit Option...");
		mnOptions.add(editOptionsMenuItem);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 447, 403, 0 };
		gbl_contentPane.rowHeights = new int[] { 251, 100, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 2.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel contractsDataPanel = new JPanel();
		contractsDataPanel.setBackground(SystemColor.control);
		GridBagConstraints gbc_contractsDataPanel = new GridBagConstraints();
		gbc_contractsDataPanel.fill = GridBagConstraints.BOTH;
		gbc_contractsDataPanel.gridheight = 2;
		gbc_contractsDataPanel.insets = new Insets(0, 0, 0, 5);
		gbc_contractsDataPanel.gridx = 0;
		gbc_contractsDataPanel.gridy = 0;
		contentPane.add(contractsDataPanel, gbc_contractsDataPanel);

		contractsTable = new JTable();
		contractsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		contractsTable.setToolTipText("Contracts");
		contractsTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Brand", "Model", "Production Year", "Contract Date"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		contractsTable.getColumnModel().getColumn(0).setResizable(false);
		contractsTable.getColumnModel().getColumn(0).setPreferredWidth(32);
		contractsTable.getColumnModel().getColumn(0).setMinWidth(32);
		contractsTable.getColumnModel().getColumn(0).setMaxWidth(32);
		contractsTable.getColumnModel().getColumn(3).setPreferredWidth(108);
		contractsTable.getColumnModel().getColumn(3).setMinWidth(60);
		contractsTable.getColumnModel().getColumn(3).setMaxWidth(108);
		contractsTable.getColumnModel().getColumn(4).setPreferredWidth(100);
		contractsTable.getColumnModel().getColumn(4).setMinWidth(50);
		contractsTable.getColumnModel().getColumn(4).setMaxWidth(120);
		contractsDataPanel.setLayout(new BorderLayout(0, 0));
		contractsTable.setBounds(0, 0, 1, 1);

		JScrollPane scrollPane = new JScrollPane(contractsTable);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		contractsDataPanel.add(scrollPane);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		contractsDataPanel.add(splitPane, BorderLayout.SOUTH);

		loadContractButton = new JButton("Load Contract");
		splitPane.setLeftComponent(loadContractButton);

		deleteContractButton = new JButton("Delete Contract");
		splitPane.setRightComponent(deleteContractButton);

		JPanel contractPanel = new JPanel();
		contractPanel.setBackground(SystemColor.control);
		GridBagConstraints gbc_contractPanel = new GridBagConstraints();
		gbc_contractPanel.insets = new Insets(0, 0, 5, 0);
		gbc_contractPanel.fill = GridBagConstraints.BOTH;
		gbc_contractPanel.gridx = 1;
		gbc_contractPanel.gridy = 0;
		contentPane.add(contractPanel, gbc_contractPanel);
		GridBagLayout gbl_contractPanel = new GridBagLayout();
		gbl_contractPanel.columnWidths = new int[] { 211, 195, 212, 0 };
		gbl_contractPanel.rowHeights = new int[] { 0, 0, 0, 0, 18, 69, 0, 0, 0, 47, 0 };
		gbl_contractPanel.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_contractPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		contractPanel.setLayout(gbl_contractPanel);

		JLabel brandLabel = new JLabel("Brand:");
		GridBagConstraints gbc_brandLabel = new GridBagConstraints();
		gbc_brandLabel.anchor = GridBagConstraints.EAST;
		gbc_brandLabel.insets = new Insets(0, 0, 5, 5);
		gbc_brandLabel.gridx = 0;
		gbc_brandLabel.gridy = 0;
		contractPanel.add(brandLabel, gbc_brandLabel);

		brandTextField = new JTextField();
		GridBagConstraints gbc_brandTextField = new GridBagConstraints();
		gbc_brandTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_brandTextField.insets = new Insets(0, 0, 5, 5);
		gbc_brandTextField.gridx = 1;
		gbc_brandTextField.gridy = 0;
		contractPanel.add(brandTextField, gbc_brandTextField);
		brandTextField.setColumns(10);

		JLabel modelLabel = new JLabel("Model:");
		GridBagConstraints gbc_modelLabel = new GridBagConstraints();
		gbc_modelLabel.anchor = GridBagConstraints.EAST;
		gbc_modelLabel.insets = new Insets(0, 0, 5, 5);
		gbc_modelLabel.gridx = 0;
		gbc_modelLabel.gridy = 1;
		contractPanel.add(modelLabel, gbc_modelLabel);

		modelTextField = new JTextField();
		GridBagConstraints gbc_modelTextField = new GridBagConstraints();
		gbc_modelTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_modelTextField.insets = new Insets(0, 0, 5, 5);
		gbc_modelTextField.gridx = 1;
		gbc_modelTextField.gridy = 1;
		contractPanel.add(modelTextField, gbc_modelTextField);
		modelTextField.setColumns(10);

		JLabel priceLabel = new JLabel("Price:");
		GridBagConstraints gbc_priceLabel = new GridBagConstraints();
		gbc_priceLabel.anchor = GridBagConstraints.EAST;
		gbc_priceLabel.insets = new Insets(0, 0, 5, 5);
		gbc_priceLabel.gridx = 0;
		gbc_priceLabel.gridy = 2;
		contractPanel.add(priceLabel, gbc_priceLabel);

		priceTextField = new JFormattedTextField();
		GridBagConstraints gbc_priceTextField = new GridBagConstraints();
		gbc_priceTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_priceTextField.insets = new Insets(0, 0, 5, 5);
		gbc_priceTextField.gridx = 1;
		gbc_priceTextField.gridy = 2;
		contractPanel.add(priceTextField, gbc_priceTextField);
		priceTextField.setColumns(10);

		JLabel prodYearLabel = new JLabel("Production Year:");
		GridBagConstraints gbc_prodYearLabel = new GridBagConstraints();
		gbc_prodYearLabel.anchor = GridBagConstraints.EAST;
		gbc_prodYearLabel.insets = new Insets(0, 0, 5, 5);
		gbc_prodYearLabel.gridx = 0;
		gbc_prodYearLabel.gridy = 3;
		contractPanel.add(prodYearLabel, gbc_prodYearLabel);

		prodYearTextField = new JFormattedTextField();
		GridBagConstraints gbc_prodYearTextField = new GridBagConstraints();
		gbc_prodYearTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_prodYearTextField.insets = new Insets(0, 0, 5, 5);
		gbc_prodYearTextField.gridx = 1;
		gbc_prodYearTextField.gridy = 3;
		contractPanel.add(prodYearTextField, gbc_prodYearTextField);
		prodYearTextField.setColumns(10);

		JLabel mileageLabel = new JLabel("Mileage:");
		GridBagConstraints gbc_mileageLabel = new GridBagConstraints();
		gbc_mileageLabel.anchor = GridBagConstraints.EAST;
		gbc_mileageLabel.insets = new Insets(0, 0, 5, 5);
		gbc_mileageLabel.gridx = 0;
		gbc_mileageLabel.gridy = 4;
		contractPanel.add(mileageLabel, gbc_mileageLabel);
		

		mileageTextField = new JFormattedTextField();
		GridBagConstraints gbc_mileageTextField = new GridBagConstraints();
		gbc_mileageTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_mileageTextField.insets = new Insets(0, 0, 5, 5);
		gbc_mileageTextField.gridx = 1;
		gbc_mileageTextField.gridy = 4;
		contractPanel.add(mileageTextField, gbc_mileageTextField);
		mileageTextField.setColumns(10);

		JLabel contractMileageLabe = new JLabel("Contract Mileage:");
		GridBagConstraints gbc_contractMileageLabe = new GridBagConstraints();
		gbc_contractMileageLabe.anchor = GridBagConstraints.SOUTHEAST;
		gbc_contractMileageLabe.insets = new Insets(0, 0, 5, 5);
		gbc_contractMileageLabe.gridx = 0;
		gbc_contractMileageLabe.gridy = 5;
		contractPanel.add(contractMileageLabe, gbc_contractMileageLabe);

		contractMileageTextField = new JFormattedTextField();
		GridBagConstraints gbc_contractMileageTextField = new GridBagConstraints();
		gbc_contractMileageTextField.anchor = GridBagConstraints.SOUTH;
		gbc_contractMileageTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_contractMileageTextField.insets = new Insets(0, 0, 5, 5);
		gbc_contractMileageTextField.gridx = 1;
		gbc_contractMileageTextField.gridy = 5;
		contractPanel.add(contractMileageTextField, gbc_contractMileageTextField);
		contractMileageTextField.setColumns(10);

		JLabel contractPriceLabel = new JLabel("Contract Price:");
		GridBagConstraints gbc_contractPriceLabel = new GridBagConstraints();
		gbc_contractPriceLabel.anchor = GridBagConstraints.EAST;
		gbc_contractPriceLabel.insets = new Insets(0, 0, 5, 5);
		gbc_contractPriceLabel.gridx = 0;
		gbc_contractPriceLabel.gridy = 6;
		contractPanel.add(contractPriceLabel, gbc_contractPriceLabel);

		contractPriceTextField = new JFormattedTextField();
		GridBagConstraints gbc_contractPriceTextField = new GridBagConstraints();
		gbc_contractPriceTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_contractPriceTextField.insets = new Insets(0, 0, 5, 5);
		gbc_contractPriceTextField.gridx = 1;
		gbc_contractPriceTextField.gridy = 6;
		contractPanel.add(contractPriceTextField, gbc_contractPriceTextField);
		contractPriceTextField.setColumns(10);

		JLabel contractStartDateLabel = new JLabel("Contract Start Date:");
		GridBagConstraints gbc_contractStartDateLabel = new GridBagConstraints();
		gbc_contractStartDateLabel.anchor = GridBagConstraints.EAST;
		gbc_contractStartDateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_contractStartDateLabel.gridx = 0;
		gbc_contractStartDateLabel.gridy = 7;
		contractPanel.add(contractStartDateLabel, gbc_contractStartDateLabel);

		MaskFormatter format = new MaskFormatter("####-##-##");
		format.setPlaceholderCharacter('_');

		contractStartDateTextField = new JFormattedTextField(format);
		GridBagConstraints gbc_contractStartDateTextField = new GridBagConstraints();
		gbc_contractStartDateTextField.insets = new Insets(0, 0, 5, 5);
		gbc_contractStartDateTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_contractStartDateTextField.gridx = 1;
		gbc_contractStartDateTextField.gridy = 7;
		contractPanel.add(contractStartDateTextField, gbc_contractStartDateTextField);

		JLabel contractEndDateLabel = new JLabel("Contract End Date:");
		GridBagConstraints gbc_contractEndDateLabel = new GridBagConstraints();
		gbc_contractEndDateLabel.anchor = GridBagConstraints.EAST;
		gbc_contractEndDateLabel.insets = new Insets(0, 0, 5, 5);
		gbc_contractEndDateLabel.gridx = 0;
		gbc_contractEndDateLabel.gridy = 8;
		contractPanel.add(contractEndDateLabel, gbc_contractEndDateLabel);

		contractEndDateTextField = new JFormattedTextField(format);
		GridBagConstraints gbc_contractEndDateTextField = new GridBagConstraints();
		gbc_contractEndDateTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_contractEndDateTextField.insets = new Insets(0, 0, 5, 5);
		gbc_contractEndDateTextField.gridx = 1;
		gbc_contractEndDateTextField.gridy = 8;
		contractPanel.add(contractEndDateTextField, gbc_contractEndDateTextField);
		contractEndDateTextField.setColumns(10);

		sumarizedLabel = new JLabel("Sumarized Contract Price:");
		GridBagConstraints gbc_sumarizedLabel = new GridBagConstraints();
		gbc_sumarizedLabel.anchor = GridBagConstraints.SOUTHEAST;
		gbc_sumarizedLabel.insets = new Insets(0, 0, 0, 5);
		gbc_sumarizedLabel.gridx = 0;
		gbc_sumarizedLabel.gridy = 9;
		contractPanel.add(sumarizedLabel, gbc_sumarizedLabel);

		sumarizedPriceLabel = new JLabel("0");
		sumarizedPriceLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GridBagConstraints gbc_sumarizedPriceLabel = new GridBagConstraints();
		gbc_sumarizedPriceLabel.insets = new Insets(0, 0, 0, 5);
		gbc_sumarizedPriceLabel.anchor = GridBagConstraints.SOUTH;
		gbc_sumarizedPriceLabel.gridx = 1;
		gbc_sumarizedPriceLabel.gridy = 9;
		contractPanel.add(sumarizedPriceLabel, gbc_sumarizedPriceLabel);

		priceTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				sumarizedPriceLabel.setText(priceTextField.getText());
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				sumarizedPriceLabel.setText(priceTextField.getText());

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				sumarizedPriceLabel.setText(priceTextField.getText());

			}
		});

		JPanel optionsPanel = new JPanel();
		optionsPanel.setBackground(SystemColor.control);
		GridBagConstraints gbc_optionsPanel = new GridBagConstraints();
		gbc_optionsPanel.fill = GridBagConstraints.BOTH;
		gbc_optionsPanel.gridx = 1;
		gbc_optionsPanel.gridy = 1;
		contentPane.add(optionsPanel, gbc_optionsPanel);
		optionsPanel.setLayout(new BorderLayout(0, 0));

		optionsTable = new JTable();
		optionsTable.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Id", "Name", "Description", "Price", "Type" }) {
			boolean[] columnEditables = new boolean[] { false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		optionsTable.getColumnModel().getColumn(0).setPreferredWidth(31);
		optionsTable.getColumnModel().getColumn(0).setMinWidth(31);
		optionsTable.getColumnModel().getColumn(0).setMaxWidth(31);
		optionsTable.getColumnModel().getColumn(2).setPreferredWidth(119);
		optionsTable.getColumnModel().getColumn(3).setPreferredWidth(55);
		optionsTable.getColumnModel().getColumn(4).setPreferredWidth(73);

		JScrollPane optionsScrollPane = new JScrollPane(optionsTable);
		optionsPanel.add(optionsScrollPane);

		Box horizontalBox = Box.createHorizontalBox();
		optionsPanel.add(horizontalBox, BorderLayout.SOUTH);

		Component horizontalStrut = Box.createHorizontalStrut(200);
		horizontalBox.add(horizontalStrut);

		JSplitPane splitPane_1 = new JSplitPane();
		horizontalBox.add(splitPane_1);
		splitPane_1.setSize(50, 10);
		splitPane_1.setResizeWeight(0.5);

		addOptionButton = new JButton("Add Option");
		splitPane_1.setLeftComponent(addOptionButton);

		deleteOptionButton = new JButton("Delete Option");
		splitPane_1.setRightComponent(deleteOptionButton);

		setVisible(true);
	}

	public void addContractToDBTable(Contract contract) {

		DefaultTableModel model = (DefaultTableModel) contractsTable.getModel();
		model.addRow(new Object[] { contract.getId(), contract.getBrand(), contract.getModel(), contract.getProdYear(),
				contract.getSigningDate() });

	}

	public void addOptionToDBTable(Option option) {

		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		model.addRow(new Object[] { option.getId(), option.getName(), option.getDescription(), option.getPrice(),
				option.getOptionType() });

	}

	public void clearContractDBTable() {
		DefaultTableModel model = (DefaultTableModel) contractsTable.getModel();
		int count = model.getRowCount();
		for (int i = 0; i < count; i++) {
			model.removeRow(0);
		}
	}

	public void clearOptionsDBTable() {
		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		int count = model.getRowCount();
		for (int i = 0; i < count; i++) {
			model.removeRow(0);
		}
	}

	public List<Integer> getOptionsIdFromUiTable() {

		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		int count = model.getRowCount();
		List<Integer> IDs = new LinkedList<Integer>();
		for (int i = 0; i < count; i++) {
			int x = (int) model.getValueAt(i, 0);
			IDs.add(x);
		}

		return IDs;

	}

	public void addMenuItemsActions(ActionListener e) {
		saveContractMenuItem.addActionListener(e);
		addingOptionMenuItem.addActionListener(e);
		newContractMenuItem.addActionListener(e);
		editOptionsMenuItem.addActionListener(e);
	}

	public void addButtonsActions(ActionListener e) {
		deleteContractButton.addActionListener(e);
		loadContractButton.addActionListener(e);
		addOptionButton.addActionListener(e);
		deleteOptionButton.addActionListener(e);
	}

	public void clearAllForNewContract() {
		brandTextField.setText(null);
		modelTextField.setText(null);
		priceTextField.setText(null);
		prodYearTextField.setText(null);
		mileageTextField.setText(null);
		contractPriceTextField.setText(null);
		contractStartDateTextField.setText(null);
		contractEndDateTextField.setText(null);
		contractMileageTextField.setText(null);
		clearOptionsDBTable();
	}

	public int getSelectedContractId() {
		if (contractsTable.getSelectedRow() < 0)
			return -1;
		return (int) contractsTable.getValueAt(contractsTable.getSelectedRow(), 0);
	}

	public int getSelectedOptionId() {
		if (optionsTable.getSelectedRow() < 0)
			return -1;
		return (int) optionsTable.getValueAt(optionsTable.getSelectedRow(), 0);
	}
	
	public void updateSumarizedPriceLabel(){
		System.out.println("Updating" +sumarizedPriceLabel);
	}

	public void deleteSelectedContract() {
		((DefaultTableModel) contractsTable.getModel()).removeRow(contractsTable.getSelectedRow());
	}

	public void deleteSelectedOption() {
		((DefaultTableModel) optionsTable.getModel()).removeRow(optionsTable.getSelectedRow());
	}
	
	public void updateSumarizedContractPrice(){
		
		List<Integer> optionPrice = new LinkedList<Integer>();
		List<String> optionType = new LinkedList<String>();
		
		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		int count = model.getRowCount();

		for (int i = 0; i < count; i++) {
			int x = (int) model.getValueAt(i, 3);
			optionPrice.add(x);
			String y = (String) model.getValueAt(i, 4);
			optionType.add(y);
		}
		
		String priceString = getPriceTextField().getText();
		int priceInt = Integer.parseInt(priceString);
		
		for(int i = 0 ; i < count ; i++){

			if(optionType.get(i).equals("BY_KM")){
				
				priceInt += optionPrice.get(i)*(Integer.parseInt(getContractMileageTextField().getText()));
				System.out.println("Adding BY_KM");
			}
			
			if(optionType.get(i).equals("BY_MONTH")){
				System.out.println("Adding BY_MONTH");
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			    Date startDate = null;
			    Date endDate = null;
				try {
					startDate = df.parse(contractStartDateTextField.getText());
					endDate = df.parse(contractEndDateTextField.getText());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			   
				
				Calendar startCalendar = new GregorianCalendar();
				startCalendar.setTime(startDate);
				Calendar endCalendar = new GregorianCalendar();
				endCalendar.setTime(endDate);
				
				int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
				int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
				
				priceInt+=diffMonth*optionPrice.get(i);
			}
			
			if(optionType.get(i).equals("FIXED")){
				System.out.println("Adding FIXED");
				priceInt+=optionPrice.get(i);
			}
		}
		
		
		sumarizedPriceLabel.setText(Integer.toString(priceInt));
		
	}

	public JTextField getBrandTextField() {
		return brandTextField;
	}

	public void setBrandTextField(String text) {
		brandTextField.setText(text);
	}

	public JTextField getModelTextField() {
		return modelTextField;
	}

	public void setModelTextField(String text) {
		this.modelTextField.setText(text);
	}

	public JTextField getPriceTextField() {
		return priceTextField;
	}

	public void setPriceTextField(int value) {
		this.priceTextField.setText(Integer.toString(value));
	}

	public JTextField getProdYearTextField() {
		return prodYearTextField;
	}

	public void setProdYearTextField(int value) {
		this.prodYearTextField.setText(Integer.toString(value));
	}

	public JTextField getMileageTextField() {
		return mileageTextField;
	}

	public void setMileageTextField(int value) {
		this.mileageTextField.setText(Integer.toString(value));
	}

	public JTextField getContractPriceTextField() {
		return contractPriceTextField;
	}

	public void setContractPriceTextField(int value) {
		this.contractPriceTextField.setText(Integer.toString(value));
	}

	public JFormattedTextField getContractStartDateTextField() {
		return contractStartDateTextField;
	}

	public void setContractStartDateTextField(String text) {
		this.contractStartDateTextField.setText(text);
	}

	public JFormattedTextField getContractEndDateTextField() {
		return contractEndDateTextField;
	}

	public void setContractEndDateTextField(String text) {
		this.contractEndDateTextField.setText(text);
	}

	public JTextField getContractMileageTextField() {
		return contractMileageTextField;
	}

	public void setContractMileageTextField(int value) {
		this.contractMileageTextField.setText(Integer.toString(value));
	}

	public JMenuItem getContractSaveMenuItem() {
		return saveContractMenuItem;
	}

	public JButton getLoadingButton() {
		return loadContractButton;
	}

	public JButton getAddOptionButton() {
		return addOptionButton;
	}

	public JButton getDeleteContractButton() {
		return deleteContractButton;
	}

	public JMenuItem getAddingOptionMenuItem() {
		return addingOptionMenuItem;
	}

	public JMenuItem getNewContractMenuItem() {
		return newContractMenuItem;
	}
	
	public JMenuItem getEditOptionMenuItem(){
		return editOptionsMenuItem;
	}

	public JButton getDeleteOptionButton() {
		return deleteOptionButton;
	}
}
