package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dbmodel.Option;

import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JDialog;

@SuppressWarnings("serial")
public class SelectingOptionsWindow extends JDialog {

	private JPanel contentPane;
	private JTable optionsTable;
	private JButton addToContractButton;
	private JButton cancelButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//SelectingOptionsWindow frame = new SelectingOptionsWindow();
				//	frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SelectingOptionsWindow(JFrame parent) {
		super(parent, true);
		setResizable(false);
		setTitle("Select Options");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 702, 389);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		optionsTable = new JTable();
		optionsTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id", "Name", "Description", "Price", "Type"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, Integer.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		optionsTable.getColumnModel().getColumn(0).setResizable(false);
		optionsTable.getColumnModel().getColumn(0).setPreferredWidth(31);
		optionsTable.getColumnModel().getColumn(0).setMinWidth(31);
		optionsTable.getColumnModel().getColumn(0).setMaxWidth(31);
		optionsTable.getColumnModel().getColumn(1).setResizable(false);
		optionsTable.getColumnModel().getColumn(1).setPreferredWidth(120);
		optionsTable.getColumnModel().getColumn(1).setMinWidth(120);
		optionsTable.getColumnModel().getColumn(1).setMaxWidth(120);
		optionsTable.getColumnModel().getColumn(2).setPreferredWidth(350);
		optionsTable.getColumnModel().getColumn(2).setMinWidth(350);
		optionsTable.getColumnModel().getColumn(2).setMaxWidth(350);
		optionsTable.getColumnModel().getColumn(3).setResizable(false);
		optionsTable.getColumnModel().getColumn(3).setPreferredWidth(100);
		optionsTable.getColumnModel().getColumn(3).setMinWidth(100);
		optionsTable.getColumnModel().getColumn(3).setMaxWidth(100);
		optionsTable.getColumnModel().getColumn(4).setResizable(false);
		optionsTable.getColumnModel().getColumn(4).setPreferredWidth(85);
		optionsTable.getColumnModel().getColumn(4).setMinWidth(85);
		optionsTable.getColumnModel().getColumn(4).setMaxWidth(85);
		scrollPane.setViewportView(optionsTable);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		contentPane.add(splitPane, BorderLayout.SOUTH);

		addToContractButton = new JButton("Add to contract");
		splitPane.setLeftComponent(addToContractButton);

		cancelButton = new JButton("Cancel");
		splitPane.setRightComponent(cancelButton);

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}

	public void addActionsToButtons(ActionListener arg) {

		addToContractButton.addActionListener(arg);
		cancelButton.addActionListener(arg);

	}

	public void addOptionToTable(Option option) {

		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		model.addRow(new Object[] { option.getId(), option.getName(), option.getDescription(), option.getPrice(),
				option.getOptionType(), option.isActive() });

	}

	public int getSelectedOptionId() {
		if (optionsTable.getSelectedRow() < 0)
			return -1;
		return (int) optionsTable.getValueAt(optionsTable.getSelectedRow(), 0);
	}

	public JButton getAddOptionToContractButton() {
		return addToContractButton;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}

}
