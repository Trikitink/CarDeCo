package gui;

import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JSplitPane;
import javax.swing.JSpinner;
import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dbmodel.Option;
import dbmodel.Option.OptionType;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;

public class EditingOptionsWindow extends JDialog implements OptionContainer{

	private JPanel contentPane;
	private JTable optionsTable;
	private JTextField nameTextField;
	private JTextArea descriptionTextArea;
	private JTextField priceTextField;
	
	private JLabel optionIdLabel;
	
	private ButtonGroup radiosGroup;
	
	private JRadioButton kmRadioButton;
	private JRadioButton monthRadioButton;
	private JRadioButton fixedRadioButton;
	
	private JCheckBox isOptionActive;
	
	private JButton cancelButton;
	private JButton saveButton;
	private JButton deleteButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//EditingOptionsWindow frame = new EditingOptionsWindow();
				//	frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditingOptionsWindow(JFrame parent) {
		super(parent, true);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 697, 341);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane);
		
		optionsTable = new JTable();
		optionsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		optionsTable.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Id", "Name", "Price", "Type", "Active"
				}
			) {
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, Integer.class, String.class, Boolean.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		optionsTable.getColumnModel().getColumn(0).setPreferredWidth(31);
		optionsTable.getColumnModel().getColumn(0).setMinWidth(31);
		optionsTable.getColumnModel().getColumn(0).setMaxWidth(31);
		optionsTable.getColumnModel().getColumn(1).setPreferredWidth(100);
		optionsTable.getColumnModel().getColumn(1).setMinWidth(100);
		optionsTable.getColumnModel().getColumn(1).setMaxWidth(100);
		optionsTable.getColumnModel().getColumn(4).setResizable(false);
		optionsTable.getColumnModel().getColumn(4).setPreferredWidth(54);
		optionsTable.getColumnModel().getColumn(4).setMinWidth(54);
		optionsTable.getColumnModel().getColumn(4).setMaxWidth(54);
		scrollPane.setViewportView(optionsTable);
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{31, 0, 0};
		gbl_panel.rowHeights = new int[]{25, 30, 111, 30, 40, 30, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel idLabel = new JLabel("Id:");
		GridBagConstraints gbc_idLabel = new GridBagConstraints();
		gbc_idLabel.anchor = GridBagConstraints.EAST;
		gbc_idLabel.insets = new Insets(0, 0, 5, 5);
		gbc_idLabel.gridx = 0;
		gbc_idLabel.gridy = 0;
		panel.add(idLabel, gbc_idLabel);
		
		optionIdLabel = new JLabel("");
		GridBagConstraints gbc_optionIdLabel = new GridBagConstraints();
		gbc_optionIdLabel.anchor = GridBagConstraints.WEST;
		gbc_optionIdLabel.insets = new Insets(0, 0, 5, 0);
		gbc_optionIdLabel.gridx = 1;
		gbc_optionIdLabel.gridy = 0;
		panel.add(optionIdLabel, gbc_optionIdLabel);
		
		JLabel nameLabel = new JLabel("Name:");
		GridBagConstraints gbc_nameLabel = new GridBagConstraints();
		gbc_nameLabel.anchor = GridBagConstraints.EAST;
		gbc_nameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nameLabel.gridx = 0;
		gbc_nameLabel.gridy = 1;
		panel.add(nameLabel, gbc_nameLabel);
		
		nameTextField = new JTextField();
		GridBagConstraints gbc_nameTextField = new GridBagConstraints();
		gbc_nameTextField.insets = new Insets(0, 0, 5, 0);
		gbc_nameTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_nameTextField.gridx = 1;
		gbc_nameTextField.gridy = 1;
		panel.add(nameTextField, gbc_nameTextField);
		nameTextField.setColumns(10);
		
		JLabel descriptionLabel = new JLabel("Description:");
		GridBagConstraints gbc_descriptionLabel = new GridBagConstraints();
		gbc_descriptionLabel.anchor = GridBagConstraints.EAST;
		gbc_descriptionLabel.insets = new Insets(0, 0, 5, 5);
		gbc_descriptionLabel.gridx = 0;
		gbc_descriptionLabel.gridy = 2;
		panel.add(descriptionLabel, gbc_descriptionLabel);
		
		descriptionTextArea = new JTextArea();
		descriptionTextArea.setRows(1);
		descriptionTextArea.setTabSize(5);
		GridBagConstraints gbc_descriptionTextArea = new GridBagConstraints();
		gbc_descriptionTextArea.fill = GridBagConstraints.BOTH;
		gbc_descriptionTextArea.insets = new Insets(0, 0, 5, 0);
		gbc_descriptionTextArea.gridx = 1;
		gbc_descriptionTextArea.gridy = 2;
		panel.add(descriptionTextArea, gbc_descriptionTextArea);
		
		JLabel priceLabel = new JLabel("Price:");
		GridBagConstraints gbc_priceLabel = new GridBagConstraints();
		gbc_priceLabel.anchor = GridBagConstraints.EAST;
		gbc_priceLabel.insets = new Insets(0, 0, 5, 5);
		gbc_priceLabel.gridx = 0;
		gbc_priceLabel.gridy = 3;
		panel.add(priceLabel, gbc_priceLabel);
		
		priceTextField = new JTextField();
		GridBagConstraints gbc_priceTextField = new GridBagConstraints();
		gbc_priceTextField.insets = new Insets(0, 0, 5, 0);
		gbc_priceTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_priceTextField.gridx = 1;
		gbc_priceTextField.gridy = 3;
		panel.add(priceTextField, gbc_priceTextField);
		priceTextField.setColumns(10);
		
		JLabel optionTypeLabel = new JLabel("Option Type:");
		GridBagConstraints gbc_optionTypeLabel = new GridBagConstraints();
		gbc_optionTypeLabel.anchor = GridBagConstraints.EAST;
		gbc_optionTypeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_optionTypeLabel.gridx = 0;
		gbc_optionTypeLabel.gridy = 4;
		panel.add(optionTypeLabel, gbc_optionTypeLabel);
		
		Box horizontalBox = Box.createHorizontalBox();
		GridBagConstraints gbc_horizontalBox = new GridBagConstraints();
		gbc_horizontalBox.anchor = GridBagConstraints.WEST;
		gbc_horizontalBox.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalBox.gridx = 1;
		gbc_horizontalBox.gridy = 4;
		panel.add(horizontalBox, gbc_horizontalBox);
		
		radiosGroup = new ButtonGroup();
		
		kmRadioButton = new JRadioButton("By Km");
		horizontalBox.add(kmRadioButton);
		
		monthRadioButton = new JRadioButton("By Month");
		horizontalBox.add(monthRadioButton);
		
		fixedRadioButton = new JRadioButton("Fixed");
		horizontalBox.add(fixedRadioButton);
		
		radiosGroup.add(kmRadioButton);
		radiosGroup.add(monthRadioButton);
		radiosGroup.add(fixedRadioButton);
		
		JLabel isActiveLabel = new JLabel("Active:");
		GridBagConstraints gbc_isActiveLabel = new GridBagConstraints();
		gbc_isActiveLabel.anchor = GridBagConstraints.EAST;
		gbc_isActiveLabel.insets = new Insets(0, 0, 5, 5);
		gbc_isActiveLabel.gridx = 0;
		gbc_isActiveLabel.gridy = 5;
		panel.add(isActiveLabel, gbc_isActiveLabel);
		
		isOptionActive = new JCheckBox("");
		GridBagConstraints gbc_isOptionActive = new GridBagConstraints();
		gbc_isOptionActive.anchor = GridBagConstraints.WEST;
		gbc_isOptionActive.insets = new Insets(0, 0, 5, 0);
		gbc_isOptionActive.gridx = 1;
		gbc_isOptionActive.gridy = 5;
		panel.add(isOptionActive, gbc_isOptionActive);
		
		deleteButton = new JButton("Delete");
		GridBagConstraints gbc_deleteButton = new GridBagConstraints();
		gbc_deleteButton.insets = new Insets(0, 0, 0, 5);
		gbc_deleteButton.gridx = 0;
		gbc_deleteButton.gridy = 6;
		panel.add(deleteButton, gbc_deleteButton);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 1;
		gbc_splitPane.gridy = 6;
		panel.add(splitPane, gbc_splitPane);

		saveButton = new JButton("Save");
		splitPane.setLeftComponent(saveButton);
		
		cancelButton = new JButton("Cancel");
		splitPane.setRightComponent(cancelButton);

	}
	
	public void addOptionToTable(Option option) {

		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		model.addRow(new Object[] { option.getId(), option.getName(), option.getPrice(),
				option.getOptionType(), option.isActive() });

	}
	
	public JTable getTable(){
		return optionsTable;
	}
	
	public String getSelectedOptionType() {
		if (kmRadioButton.isSelected())
			return "BY_KM";
		if (monthRadioButton.isSelected())
			return "BY_MONTH";
		if (fixedRadioButton.isSelected())
			return "FIXED";

		return null;
	}

	public JTable getOptionsTable() {
		return optionsTable;
	}

	public void setOptionsTable(JTable optionsTable) {
		this.optionsTable = optionsTable;
	}

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(String text) {
		nameTextField.setText(text);
	}

	public JTextArea getDescriptionTextArea() {
		return descriptionTextArea;
	}

	public void setDescriptionTextArea(String text) {
		descriptionTextArea.setText(text);
	}

	public JTextField getPriceTextField() {
		return priceTextField;
	}

	public void setPriceTextField(String text) {
		priceTextField.setText(text);
	}

	public JLabel getOptionIdLabel() {
		return optionIdLabel;
	}

	public void setOptionIdLabel(String text) {
		optionIdLabel.setText(text);
	}
	
	public int getSelectedOptionId() {
		if (optionsTable.getSelectedRow() < 0)
			return -1;
		return (int) optionsTable.getValueAt(optionsTable.getSelectedRow(), 0);
	}

	public boolean getIsOptionActive() {
		if (isOptionActive.isSelected()) return true;
		else return false;
	}

	public void setIsOptionActive(boolean isActive) {
		isOptionActive.setSelected(isActive);
	}
	
	public void addMouseListenerToTable(MouseListener e){
		optionsTable.addMouseListener(e);
	}
	
	public void addActionsForButtons(ActionListener e){
		
		saveButton.addActionListener(e);
		cancelButton.addActionListener(e);
		deleteButton.addActionListener(e);
		
	}
	
	public void setRadioButton(OptionType text){
		switch(text) {
		case BY_KM: {
			kmRadioButton.setSelected(true);
			break;
			}
		case BY_MONTH :{
			monthRadioButton.setSelected(true);
			}
		case FIXED :{
			fixedRadioButton.setSelected(true);
			}
		}
		return;

	}
	
	public JButton getCancelButton(){
		return cancelButton;
	}
	
	public JButton getSaveButton(){
		return saveButton;
	}
	
	public JButton getDeleteButton(){
		return deleteButton;
	}
	
	public void clearOptionsDBTable() {
		DefaultTableModel model = (DefaultTableModel) optionsTable.getModel();
		int count = model.getRowCount();
		for (int i = 0; i < count; i++) {
			model.removeRow(0);
		}
	}
	
	public void deleteSelectedOption() {
		((DefaultTableModel) optionsTable.getModel()).removeRow(optionsTable.getSelectedRow());
	}

}
