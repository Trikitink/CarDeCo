package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.alee.laf.WebLookAndFeel;

import dbmodel.Option.OptionType;

import java.awt.GridBagLayout;
import javax.swing.Box;
import javax.swing.ButtonGroup;

import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.Component;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JDialog;

@SuppressWarnings("serial")
public class AddingOptionsWindow extends JDialog implements OptionContainer{

	private JPanel contentPane;
	private JTextField nameTextField;
	private JTextField priceTextField;

	private JTextArea descriptionTextArea;

	private JButton addButton;
	private JButton cancelButton;

	private JRadioButton forKmRadioButton;
	private JRadioButton monthlyRadioButton;
	private JRadioButton fixedRadioButton;

	private ButtonGroup optionTypesRadios;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(WebLookAndFeel.class.getCanonicalName());
					//AddingOptionsWindow frame = new AddingOptionsWindow();
				//	frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddingOptionsWindow(JFrame parent) {
		super(parent, true);
		
		setTitle("Adding Option");
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 370, 285);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 98, 56, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		Box nameBox = Box.createHorizontalBox();
		GridBagConstraints gbc_nameBox = new GridBagConstraints();
		gbc_nameBox.anchor = GridBagConstraints.WEST;
		gbc_nameBox.insets = new Insets(0, 0, 5, 0);
		gbc_nameBox.gridx = 0;
		gbc_nameBox.gridy = 0;
		contentPane.add(nameBox, gbc_nameBox);

		JLabel nameLabel = new JLabel("Name:");
		nameBox.add(nameLabel);

		Component horizontalStrut_1 = Box.createHorizontalStrut(46);
		nameBox.add(horizontalStrut_1);

		nameTextField = new JTextField();
		nameTextField.setToolTipText("Provide option name");
		nameTextField.setColumns(10);
		nameTextField.setAlignmentX(1.0f);
		nameBox.add(nameTextField);

		Component horizontalGlue = Box.createHorizontalGlue();
		nameBox.add(horizontalGlue);

		Box priceBox = Box.createHorizontalBox();
		GridBagConstraints gbc_priceBox = new GridBagConstraints();
		gbc_priceBox.anchor = GridBagConstraints.WEST;
		gbc_priceBox.insets = new Insets(0, 0, 5, 0);
		gbc_priceBox.gridx = 0;
		gbc_priceBox.gridy = 1;
		contentPane.add(priceBox, gbc_priceBox);

		JLabel priceLabel = new JLabel("Price:");
		priceBox.add(priceLabel);

		Component horizontalStrut_2 = Box.createHorizontalStrut(51);
		priceBox.add(horizontalStrut_2);

		priceTextField = new JTextField();
		priceTextField.setColumns(10);
		priceBox.add(priceTextField);

		Box descriptionBox = Box.createHorizontalBox();
		GridBagConstraints gbc_descriptionBox = new GridBagConstraints();
		gbc_descriptionBox.anchor = GridBagConstraints.NORTHWEST;
		gbc_descriptionBox.insets = new Insets(0, 0, 5, 0);
		gbc_descriptionBox.gridx = 0;
		gbc_descriptionBox.gridy = 2;
		contentPane.add(descriptionBox, gbc_descriptionBox);

		JLabel descriptionLabel = new JLabel("Description:");
		descriptionBox.add(descriptionLabel);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		descriptionBox.add(horizontalStrut);

		descriptionTextArea = new JTextArea();
		descriptionTextArea.setLineWrap(true);
		descriptionTextArea.setRows(5);
		descriptionTextArea.setColumns(25);
		descriptionBox.add(descriptionTextArea);

		Box optionTypeBox = Box.createHorizontalBox();
		GridBagConstraints gbc_optionTypeBox = new GridBagConstraints();
		gbc_optionTypeBox.insets = new Insets(0, 0, 5, 0);
		gbc_optionTypeBox.anchor = GridBagConstraints.WEST;
		gbc_optionTypeBox.gridx = 0;
		gbc_optionTypeBox.gridy = 3;
		contentPane.add(optionTypeBox, gbc_optionTypeBox);

		JLabel optionTypeLabel = new JLabel("Option Type:");
		optionTypeLabel.setToolTipText("How client should be charged for option");
		optionTypeBox.add(optionTypeLabel);

		Component horizontalStrut_3 = Box.createHorizontalStrut(50);
		optionTypeBox.add(horizontalStrut_3);

		forKmRadioButton = new JRadioButton("For Km");
		forKmRadioButton.setAlignmentX(1.0f);
		optionTypeBox.add(forKmRadioButton);

		Component horizontalStrut_4 = Box.createHorizontalStrut(20);
		optionTypeBox.add(horizontalStrut_4);

		monthlyRadioButton = new JRadioButton("Monthly");
		monthlyRadioButton.setAlignmentX(1.0f);
		optionTypeBox.add(monthlyRadioButton);

		Component horizontalStrut_5 = Box.createHorizontalStrut(20);
		optionTypeBox.add(horizontalStrut_5);

		fixedRadioButton = new JRadioButton("Fixed");
		fixedRadioButton.setAlignmentX(1.0f);
		optionTypeBox.add(fixedRadioButton);

		optionTypesRadios = new ButtonGroup();
		optionTypesRadios.add(forKmRadioButton);
		optionTypesRadios.add(monthlyRadioButton);
		optionTypesRadios.add(fixedRadioButton);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.anchor = GridBagConstraints.SOUTH;
		gbc_splitPane.fill = GridBagConstraints.HORIZONTAL;
		gbc_splitPane.gridx = 0;
		gbc_splitPane.gridy = 4;
		contentPane.add(splitPane, gbc_splitPane);

		addButton = new JButton("Add");
		splitPane.setLeftComponent(addButton);

		cancelButton = new JButton("Cancel");
		splitPane.setRightComponent(cancelButton);

	}

	public void addActionsToButtons(ActionListener e) {

		addButton.addActionListener(e);
		cancelButton.addActionListener(e);

	}

	public JButton getAddButton() {
		return addButton;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}

	public JTextField getNameTextField() {
		return nameTextField;
	}

	public void setNameTextField(String text) {
		nameTextField.setText(text);
	}

	public JTextField getPriceTextField() {
		return priceTextField;
	}

	public void setPriceTextField(String text) {
		priceTextField.setText(text);
	}

	public JTextArea getDescriptionTextArea() {
		return descriptionTextArea;
	}

	public void setDescriptionTextArea(String text) {
		descriptionTextArea.setText(text);
	}

	public JRadioButton getForKmRadioButton() {
		return forKmRadioButton;
	}

	public void setForKmRadioButton(JRadioButton forKmRadioButton) {
		this.forKmRadioButton = forKmRadioButton;
	}

	public JRadioButton getMonthlyRadioButton() {
		return monthlyRadioButton;
	}

	public void setMonthlyRadioButton(JRadioButton monthlyRadioButton) {
		this.monthlyRadioButton = monthlyRadioButton;
	}

	public JRadioButton getFixedRadioButton() {
		return fixedRadioButton;
	}

	public void setFixedRadioButton(JRadioButton fixedRadioButton) {
		this.fixedRadioButton = fixedRadioButton;
	}

	public String getSelectedOptionType() {
		if (forKmRadioButton.isSelected())
			return "BY_KM";
		if (monthlyRadioButton.isSelected())
			return "BY_MONTH";
		if (fixedRadioButton.isSelected())
			return "FIXED";

		return null;
	}

	public void setRadioButton(OptionType text){
		switch(text) {
		case BY_KM: {
			forKmRadioButton.setSelected(true);
			break;
			}
		case BY_MONTH :{
			monthlyRadioButton.setSelected(true);
			}
		case FIXED :{
			fixedRadioButton.setSelected(true);
			}
		}
		return;

	}

	@Override
	public void setIsOptionActive(boolean active) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setOptionIdLabel(String value) {
		// TODO Auto-generated method stub
	}

}
