package gui;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import dbmodel.Option.OptionType;

public interface OptionContainer {
	
	public String getSelectedOptionType();
	public JTextField getNameTextField();
	public JTextArea getDescriptionTextArea();
	public JTextField getPriceTextField();
	public void setOptionIdLabel(String value);
	public void setNameTextField(String value);
	public void setDescriptionTextArea(String value);
	public void setPriceTextField(String value);
	public void setRadioButton(OptionType radio);
	public void setIsOptionActive(boolean active);

}
