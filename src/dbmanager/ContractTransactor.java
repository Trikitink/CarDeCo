package dbmanager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import dbmodel.Contract;

public class ContractTransactor extends BasicDatabaseTransactor<Contract> {

	@Override
	protected String createTableStatement() {
		String createStatement = "CREATE TABLE IF NOT EXISTS contracts (id INTEGER PRIMARY KEY AUTOINCREMENT, brand varchar(255), "
				+ "model varchar(255), price int, prodYear int, carMileage int, contractPrice int, contractMileage int, signingDate date, "
				+ "contractStartDate date, contractEndDate date)";
		return createStatement;
	}


	@Override
	protected PreparedStatement insertStatement(Contract obj) throws SQLException {
		PreparedStatement prepStmt = conn.prepareStatement(
				"insert into contracts values (NULL, ?, ?, ?, ?, ?, ?, ?, date(?), date(?), date(?));");
		prepStmt.setString(1, obj.getBrand());
		prepStmt.setString(2, obj.getModel());
		prepStmt.setInt(3, obj.getPrice());
		prepStmt.setInt(4, obj.getProdYear());
		prepStmt.setInt(5, obj.getMileage());
		prepStmt.setInt(6, obj.getContractMileage());
		prepStmt.setInt(7, obj.getContractPrice());
		prepStmt.setString(8, obj.getSigningDate());
		prepStmt.setString(9, obj.getContractStartDate());
		prepStmt.setString(10, obj.getContractEndDate());

		return prepStmt;
	}

	@Override
	protected String getRowsStatement() throws SQLException {
		return "SELECT * FROM contracts order by id desc;";
	}

	@Override
	protected PreparedStatement deleteStatement(int id) throws SQLException {
		PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM options where id = ?;");
		prepStmt.setInt(1, id);
		return prepStmt;
	}

	@Override
	protected PreparedStatement updateStatement(Contract obj) throws SQLException {
		PreparedStatement prepStmt = conn.prepareStatement("UPDATE contracts SET brand=?, model=?,"
				+ "price=?, prodYear=?, carMileage=?, contractMileage=?, contractPrice=?, "
				+ "signingDate=?, contractStartDate=?, contractEndDate=? where ID = ?;");
		prepStmt.setString(1, obj.getBrand());
		prepStmt.setString(2, obj.getModel());
		prepStmt.setInt(3, obj.getPrice());
		prepStmt.setInt(4, obj.getProdYear());
		prepStmt.setInt(5, obj.getMileage());
		prepStmt.setInt(6, obj.getContractMileage());
		prepStmt.setInt(7, obj.getContractPrice());
		prepStmt.setString(8, obj.getSigningDate());
		prepStmt.setString(9, obj.getContractStartDate());
		prepStmt.setString(10, obj.getContractEndDate());
		prepStmt.setInt(11, obj.getId());
		
		return prepStmt;
	}


	@Override
	protected ResultSet selectAllResult() throws SQLException {
		return stat.executeQuery("SELECT * FROM contracts");
	}


	@Override
	protected Contract createObjectFromResult(ResultSet result) {
		try {
			return new Contract(result.getInt("id"), result.getString("brand"), result.getString("model"), 
					result.getInt("price"), result.getInt("prodYear"), result.getInt("carMileage"), result.getInt("contractMileage"), 
					result.getInt("contractPrice"),	result.getString("signingDate"), result.getString("contractStartDate"), result.getString("contractEndDate"));
		} catch (SQLException e) {
			System.err.println("Something broke on createObjectFromResult in ContractTransactor");
			JOptionPane.showConfirmDialog(null, "Something broke on createObjectFromResult in ContractTransactor", "Warning",
					JOptionPane.DEFAULT_OPTION);
			e.printStackTrace();
			return null;
		}
	}


	@Override
	protected ResultSet selectByIdResult(int id) throws SQLException {
		return stat.executeQuery("SELECT * FROM contracts where id = " + id);
	}

	

}
