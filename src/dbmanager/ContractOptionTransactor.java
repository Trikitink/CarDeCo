package dbmanager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;

import dbmodel.ContractOption;
import dbmodel.Option;
import dbmodel.Option.OptionType;

public class ContractOptionTransactor extends BasicDatabaseTransactor<ContractOption>{

	@Override
	protected String createTableStatement() {

		 String createStatement = "CREATE TABLE IF NOT EXISTS contractsoptions (id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "contract_id int, option_id int, FOREIGN KEY (contract_id) REFERENCES contracts(id), FOREIGN KEY (option_id) REFERENCES options(id))";
		 
		 return createStatement;
	}
	
	public boolean deleteConnectionBetween(int conId, int optId){
		try {
			PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM contractsoptions where option_id = "
					+ optId + " and contract_id = " + conId + ";");
			prepStmt.execute();

		} catch (SQLException e) {
			System.err.println("Error in deleting contracts option");
			return false;
		}
		return true;
	}

	@Override
	protected PreparedStatement insertStatement(ContractOption obj) throws SQLException {
		
		PreparedStatement prepStmt = conn.prepareStatement("insert into contractsoptions values (NULL, ?, ?);");
		prepStmt.setInt(1, obj.getContract_id());
		prepStmt.setInt(2, obj.getContract_id());
		
		return prepStmt;
	}

	@Override
	protected String getRowsStatement() throws SQLException {
		return "SELECT * FROM contractsoptions order by id desc;";
	}

	@Override
	protected PreparedStatement deleteStatement(int id) throws SQLException {
		PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM contracts where id = ?;");
		prepStmt.setInt(1, id);
		return prepStmt;
	}

	@Override
	protected PreparedStatement updateStatement(ContractOption obj) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResultSet selectAllResult() throws SQLException {
		return stat.executeQuery("SELECT * FROM contractsoptions");
	}

	@Override
	protected ContractOption createObjectFromResult(ResultSet result) {
		try {
			return new ContractOption(result.getInt("id"), result.getInt("contract_id"), result.getInt("option_id"));
		} catch (SQLException e) {
			System.err.println("Something broke on createObjectFromResult in ContractOptionTransactor");
			JOptionPane.showConfirmDialog(null, "Something broke on createObjectFromResult in ContractOptionTransactor", "Warning",
					JOptionPane.DEFAULT_OPTION);
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected ResultSet selectByIdResult(int id) throws SQLException {
		return stat.executeQuery("SELECT * FROM contractsoptions WHERE option_id = " + id);
	}

	public List<Option> selectContractsOptionsByContractId(int id) {
		List<Option> option = new LinkedList<Option>();
		try {
			ResultSet result = stat.executeQuery(
					"SELECT * FROM options INNER JOIN contractsoptions ON contractsoptions.option_id=options.id WHERE contractsoptions.contract_id = "
							+ id);
			int opt_id, price;
			String name, description, optionType;
			boolean active;

			while (result.next()) {
				opt_id = result.getInt("id");
				name = result.getString("name");
				description = result.getString("description");
				price = result.getInt("price");
				optionType = result.getString("optionType");
				active = result.getBoolean("active");

				option.add(new Option(opt_id, name, description, price, OptionType.valueOf(optionType), active));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return option;
	}
	
	public ContractOption selectSingleContractOptions(int optionId, int contractId) {
		ContractOption contractsoptions = null;
		try {
			ResultSet result = stat.executeQuery("SELECT * FROM contractsoptions WHERE option_id = " + optionId
					+ " and contract_id = " + contractId);
			int id, contract_id, option_id;
			while (result.next()) {
				id = result.getInt("id");
				contract_id = result.getInt("contract_id");
				option_id = result.getInt("option_id");
				contractsoptions = new ContractOption(id, contract_id, option_id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return contractsoptions;
	}

}
