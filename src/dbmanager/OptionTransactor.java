package dbmanager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import dbmodel.Option;
import dbmodel.Option.OptionType;

public class OptionTransactor extends BasicDatabaseTransactor<Option>{

	@Override
	protected String createTableStatement() {

		String createStatement = "CREATE TABLE IF NOT EXISTS options (id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255), "
				+ "description varchar(255), price int, optionType varchar(10), active bool)";
		
		return createStatement;
	}


	@Override
	protected PreparedStatement insertStatement(Option obj) throws SQLException {
		
		PreparedStatement prepStmt = conn.prepareStatement("insert into options values (NULL, ?, ?, ?, ?, ?);");
		prepStmt.setString(1, obj.getName());
		prepStmt.setString(2, obj.getDescription());
		prepStmt.setInt(3, obj.getPrice());
		prepStmt.setString(4, obj.getOptionType().name());
		prepStmt.setBoolean(5, obj.isActive());
		
		return prepStmt;
	}

	@Override
	protected String getRowsStatement() throws SQLException {
		return "SELECT * FROM options order by id desc;";
	}

	@Override
	protected PreparedStatement deleteStatement(int id) throws SQLException {
		PreparedStatement prepStmt = conn.prepareStatement("DELETE FROM options where id = ?;");
		prepStmt.setInt(1, id);
		return prepStmt;
	}

	@Override
	protected PreparedStatement updateStatement(Option obj) throws SQLException {
		
		PreparedStatement prepStmt = conn.prepareStatement("UPDATE options SET name=?, description=?,"
				+ "price=?, optionType=?, active=? where ID = ?;");
		prepStmt.setString(1, obj.getName());
		prepStmt.setString(2, obj.getDescription());
		prepStmt.setInt(3, obj.getPrice());		
		prepStmt.setString(4, obj.getOptionType().name());
		prepStmt.setBoolean(5, obj.isActive());
		prepStmt.setInt(6, obj.getId());
		
		return prepStmt;
	}


	@Override
	protected ResultSet selectAllResult() throws SQLException {
		return stat.executeQuery("SELECT * FROM options");
	}


	@Override
	protected Option createObjectFromResult(ResultSet result) {
		try {
			return new Option(result.getInt("id"), result.getString("name"), result.getString("description"), 
					result.getInt("price"), OptionType.valueOf(result.getString("optionType")), result.getBoolean("active"));
		} catch (SQLException e) {
			System.err.println("Something broke on createObjectFromResult in OptionTransactions");
			JOptionPane.showConfirmDialog(null, "Something broke on createObjectFromResult in OptionTransactions", "Warning",
					JOptionPane.DEFAULT_OPTION);
			e.printStackTrace();
			return null;
		}

	}


	@Override
	protected ResultSet selectByIdResult(int id) throws SQLException {
		return stat.executeQuery("SELECT * FROM options where id = " + id);
	}


}
