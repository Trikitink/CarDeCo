package dbmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import dbmodel.BasicDatabaseObject;

public abstract class BasicDatabaseTransactor<T extends BasicDatabaseObject> {
	
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:cardeco.db";
	
	protected Connection conn;
	protected Statement stat;
	
	public BasicDatabaseTransactor(){	
			try {
				Class.forName(BasicDatabaseTransactor.DRIVER);
			} catch (ClassNotFoundException e) {
				System.err.println("Brak sterownika JDBC");
				e.printStackTrace();
			}

			try {
				conn = DriverManager.getConnection(DB_URL);
				stat = conn.createStatement();
			} catch (SQLException e) {
				System.err.println("Problem z otwarciem polaczenia");
				e.printStackTrace();
			}

			createTable();
	}
	private boolean createTable(){
		try {
			String create = createTableStatement();
			stat.execute(create);
		} catch (SQLException e) {
			System.err.println("Blad przy tworzeniu tabeli");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insert(T obj){
		try {
			
			PreparedStatement prepStmt = insertStatement(obj);
			prepStmt.execute();

		} catch (SQLException e) {
			System.err.println("Blad przy dodawaniu opcji");
			return false;
		}
		return true;
	}
	
	
	public boolean update(T obj){
		try {
			PreparedStatement prepStmt = updateStatement(obj);
			prepStmt.execute();

		} catch (SQLException e) {
			System.err.println("Error in editing Contract");
			return false;
		}
		return true;
	}
	
	public int getRowCount(){
		ResultSet result;
		try {
			result = stat.executeQuery(getRowsStatement());
			if (result.next()) {
				return result.getInt("id");
			} else
				return 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	
	public boolean deleteById(int id){
		try {
			PreparedStatement prepStmt = deleteStatement(id); 
			prepStmt.execute();

		} catch (SQLException e) {
			System.err.println("Error in deleting");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public T selectById(int id){
	T object = null;
	try {
		ResultSet result = selectByIdResult(id);

		while (result.next()) {
			
			object = createObjectFromResult(result);
			
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	}
	return object;
	}
	
	
	public List<T> selectAll(){
		
		List<T> objects = new LinkedList<T>();
		
		try {
			
			ResultSet result = selectAllResult();

			while (result.next()) {
				
				objects.add(createObjectFromResult(result));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return objects;
	}
	
	protected abstract PreparedStatement insertStatement(T obj) throws SQLException;
	protected abstract PreparedStatement deleteStatement(int id) throws SQLException;
	protected abstract PreparedStatement updateStatement(T obj) throws SQLException;
	protected abstract ResultSet selectAllResult() throws SQLException;
	protected abstract ResultSet selectByIdResult(int id) throws SQLException;
	
	protected abstract T createObjectFromResult(ResultSet res);
	
	protected abstract String createTableStatement();	
	
	
	
	protected abstract String getRowsStatement() throws SQLException;
	
}
