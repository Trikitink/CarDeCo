package logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

import dbmodel.Contract;
import dbmodel.Option;
import dbmodel.Option.OptionType;
import gui.MainContractWindow;
import gui.OptionContainer;

public class LoGuiHelper {
	
	public Option getOptionFromUI(OptionContainer dialogBox) throws ParseException, WrongDataException{
		
		String optionName;
		String optionDescription;
		int price = -1;
		
		optionName = dialogBox.getNameTextField().getText();
		if(optionName.isEmpty()){
			throw new WrongDataException("Option name can't be empty");
		}
		
		optionDescription = dialogBox.getDescriptionTextArea().getText();
		if(optionDescription.isEmpty()){
			throw new WrongDataException("Option description can't be empty");
		}
		
		try{
		price = Integer.parseInt(dialogBox.getPriceTextField().getText());
		} catch (NumberFormatException e){
			throw new WrongDataException("Wrong price format");
		}
		if (price < 0){
			throw new WrongDataException("Price can't be less than 0");
		}
		
		OptionType optionType = OptionType.valueOf(dialogBox.getSelectedOptionType());
		
		return new Option(0,optionName, optionDescription, price, optionType, true);
	}
	
	public void passOptionToUI(OptionContainer dialogBox, Option opt){
		
		dialogBox.setOptionIdLabel(Integer.toString(opt.getId()));
		dialogBox.setNameTextField(opt.getName());
		dialogBox.setDescriptionTextArea(opt.getDescription());
		dialogBox.setPriceTextField(Integer.toString(opt.getPrice()));
		dialogBox.setRadioButton(opt.getOptionType());
		dialogBox.setIsOptionActive(opt.isActive());
		
	}
	
	public Contract getContractFromUI(MainContractWindow window, Contract update) throws ParseException, WrongDataException{
		
		boolean isUpdate = (update!=null)?true:false;
		
		int price = -1;
		int prodYear = 0;
		int carMileage = -1;
		int contractPrice = -1;
		int contractMileage = -1;
		
		final Calendar c = Calendar.getInstance();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();
		
		Date startDate = null;
		Date endDate = null;
		
		String brand = window.getBrandTextField().getText();
		if(brand.isEmpty()){
			throw new WrongDataException("No Brand, or wrong format");
		}
		
		String model = window.getModelTextField().getText();
		if(model.isEmpty()){
			throw new WrongDataException("No Model, or wrong format");
		}
		
		
		try{
		price = Integer.parseInt(window.getPriceTextField().getText());
		} catch (NumberFormatException e){
			throw new WrongDataException("Wrong Price format");
		}
		if(price<0){
			throw new WrongDataException("Wrong Price value");
		}
		
		try{
		prodYear = Integer.parseInt(window.getProdYearTextField().getText());
		} catch (NumberFormatException e){
			throw new WrongDataException("Wrong Production Year format");
		}
		if(prodYear>c.get(Calendar.YEAR)){
			throw new WrongDataException("Wrong Production Year");
		}
		
		try{
		carMileage = Integer.parseInt(window.getMileageTextField().getText());
		} catch (NumberFormatException e){
			throw new WrongDataException("Wrong Mileage format");
		}
		if (carMileage < 0){
			throw new WrongDataException("Wrong Mileage");
		}
		
		try{
		contractPrice = Integer.parseInt(window.getContractPriceTextField().getText());
		} catch (NumberFormatException e){
			throw new WrongDataException("Wrong Contract Price format");
		}
		if (contractPrice < 0){
			throw new WrongDataException("Wrong Contract Price");
		}
		
		try{
		contractMileage = Integer.parseInt(window.getContractMileageTextField().getText());
		} catch (NumberFormatException e){
			throw new WrongDataException("Wrong Contract Mileage format");
		}
		if (contractMileage < 0){
			throw new WrongDataException("Wrong Contract Mileage");
		}
		
		try{
		startDate = dateFormat.parse(window.getContractStartDateTextField().getText());
		} catch (ParseException e){
			throw new WrongDataException("Wrong start date format, or empty date field");
		}
		if(startDate.before(today) && (!isUpdate)){
			throw new WrongDataException("Contract can't start in past");
		}
		
		try{
		endDate = dateFormat.parse(window.getContractEndDateTextField().getText());
		} catch (ParseException e){
			throw new WrongDataException("Wrong end date format, or empty date field");
		}
		if(endDate.before(startDate) && (!isUpdate)){
			throw new WrongDataException("Contract can't end before start");
		} else if (startDate.before(today) && (!isUpdate)){
			throw new WrongDataException("Contract can't end in past");
		}
		
		String signingDate = dateFormat.format(today);
		String contractStartDate = dateFormat.format(startDate);
		String contractEndDate = dateFormat.format(endDate);
		
		return new Contract(-1, brand, model, price, prodYear,
				carMileage, contractMileage, contractPrice, signingDate, contractStartDate,
				contractEndDate);
	}
	
	public void passContractToUi(MainContractWindow window, Contract con){
		
		window.setBrandTextField(con.getBrand());
		window.setModelTextField(con.getModel());
		window.setPriceTextField(con.getPrice());
		window.setProdYearTextField(con.getProdYear());
		window.setMileageTextField(con.getMileage());

		window.setContractPriceTextField(con.getContractPrice());
		window.setContractMileageTextField(con.getContractMileage());
		window.setContractStartDateTextField(con.getContractStartDate());
		window.setContractEndDateTextField(con.getContractEndDate());
		
	}
	
	public int askQuestion(String question){
		return JOptionPane.showOptionDialog(null, question,
				"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null,
				new String[] { "Yes I do", "No I don't" }, "default");
	}
	
	public void showWarning(String warning){
		JOptionPane.showConfirmDialog(null, warning, "Warning",
				JOptionPane.DEFAULT_OPTION);
	}

}
