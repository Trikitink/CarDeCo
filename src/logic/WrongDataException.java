package logic;

import javax.swing.JOptionPane;

public class WrongDataException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongDataException(String warning){
		JOptionPane.showConfirmDialog(null, warning, "Warning",
				JOptionPane.DEFAULT_OPTION);
	}
}
