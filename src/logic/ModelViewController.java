package logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JOptionPane;

import dbmanager.ContractOptionTransactor;
import dbmanager.ContractTransactor;
import dbmanager.OptionTransactor;
import dbmodel.Contract;
import dbmodel.ContractOption;
import dbmodel.Option;
import gui.AddingOptionsWindow;
import gui.EditingOptionsWindow;
import gui.MainContractWindow;
import gui.SelectingOptionsWindow;

public class ModelViewController {
	
	private LoGuiHelper helpingService = new LoGuiHelper();

	private MainContractWindow ui;
	private AddingOptionsWindow adding;
	private SelectingOptionsWindow selecting;
	private EditingOptionsWindow editing;

	private ContractTransactor contractor = new ContractTransactor();
	private OptionTransactor optionator = new OptionTransactor();
	private ContractOptionTransactor connector = new ContractOptionTransactor();

	private Contract currentlyProcessedContract;
	private Option curentlyProcessedOption;

	public ModelViewController(MainContractWindow ui) {

		this.ui = ui;

		ui.addMenuItemsActions(new MenuBarItemsActions());
		ui.addButtonsActions(new MainContractWindowButtonsActions());

		addContractsFromDBToUi();

	}

	private void refreshContractsDBinMainWindow() {
		ui.clearContractDBTable();
		addContractsFromDBToUi();
	}

	private void addContractsFromDBToUi() {

		List<Contract> contracts = contractor.selectAll();

		for (Contract d : contracts) {
			ui.addContractToDBTable(d);
		}

	}

	// CLASS CONTAINS ACTIONS FOR BUTTONS IN CONTRACT MAIN WINDOW

	class MainContractWindowButtonsActions implements ActionListener {

		@SuppressWarnings("static-access")
		@Override
		public void actionPerformed(ActionEvent arg) {

			// ACTION FOR LOAD BUTTON IN MAIN CONTRACT UI

			if (arg.getSource() == ui.getLoadingButton()) {
				ui.clearOptionsDBTable();
				currentlyProcessedContract = contractor.selectById(ui.getSelectedContractId());
				
				helpingService.passContractToUi(ui, currentlyProcessedContract);

				// Loading connected Options

				List<Option> optionsForContract = new LinkedList<Option>();		
				
				optionsForContract = connector.selectContractsOptionsByContractId(ui.getSelectedContractId());
			
				for (Option option : optionsForContract) {
					ui.addOptionToDBTable(option);
				}
				
				ui.updateSumarizedContractPrice();
			};
			
			/*
			 * END OF ACTION FOR LOAD BUTTON IN MAIN CONTRACT UI
			 */

			/*
			 * ACTION FOR DELETE BUTTON IN MAIN CONTRACT UI
			 */

			if (arg.getSource() == ui.getDeleteContractButton()) {
				if (ui.getSelectedContractId() < 0) {
					helpingService.showWarning("You didn't select any contract.");
					return;
				}

				int confirmation = helpingService.askQuestion("Do you want to delete this contract?");
				
				if (!(confirmation == JOptionPane.YES_OPTION)){
					return;
				} else {

					contractor.deleteById(ui.getSelectedContractId());
					ui.deleteSelectedContract();
				
				}

			};
			/*
			 * END OF ACTION FOR DELETE BUTTON IN MAIN CONTRACT UI
			 */

			/*
			 * ACTION FOR SELECTING OPTIONS TO CONTRACT IN MAIN CONTRACT UI
			 */

			if (arg.getSource() == ui.getAddOptionButton()) {

				selecting = new SelectingOptionsWindow(ui);
				selecting.setLocationRelativeTo(ui);
				List<Option> options = optionator.selectAll();
				for (Option opt : options) {
					if (opt.isActive())
						selecting.addOptionToTable(opt);
				}
				selecting.addActionsToButtons(new SelectingOptionsWindowActions());
				selecting.setVisible(true);

			};
			/*
			 * END OF ACTION FOR SELECTING OPTIONS TO CONTRACT IN MAIN CONTRACT
			 * UI
			 */

			/*
			 * ACTION FOR DELETING OPTIONS FROM CONTRACT IN MAIN CONTRACT UI
			 */

			if (arg.getSource() == ui.getDeleteOptionButton()) {

				if (ui.getSelectedOptionId() < 0) {
					new JOptionPane().showConfirmDialog(null, "You didn't select any contract.", "Warning",
							JOptionPane.DEFAULT_OPTION);
					return;
				} else {

					if (currentlyProcessedContract != null) {
						System.out.println(ui.getSelectedOptionId() + "  " + currentlyProcessedContract.getId());
						connector.deleteConnectionBetween(ui.getSelectedOptionId(),
								currentlyProcessedContract.getId());
					}
					ui.deleteSelectedOption();
					ui.updateSumarizedContractPrice();
				}

			}
			;
			/*
			 * END OF DELETING OPTIONS FROM CONTRACT IN MAIN CONTRACT UI
			 */

		}

	}

	/*
	 * Class contains actions for menu bar items
	 */

	class MenuBarItemsActions implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg) {

			/*
			 * Actions for Save menu item in main contract window
			 */
			if (arg.getSource() == ui.getContractSaveMenuItem()) {
				
				Contract contractToBeProcessed = null;
				
				ContractOption toBeConnected = null;
				
				try {
					contractToBeProcessed = helpingService.getContractFromUI(ui, currentlyProcessedContract);
				} catch (ParseException e1) {

					e1.printStackTrace();
				} catch (WrongDataException e){
					return;
				}

					if (currentlyProcessedContract != null) {

						contractToBeProcessed.setId(currentlyProcessedContract.getId());
						contractor.update(contractToBeProcessed);
						refreshContractsDBinMainWindow();
						List<Integer> optionsToAdd = ui.getOptionsIdFromUiTable();
						if (optionsToAdd != null) {
							for (Integer toAdd : optionsToAdd) {
								if (connector.selectSingleContractOptions(toAdd,
										currentlyProcessedContract.getId()) == null)
									
									toBeConnected = new ContractOption(0, currentlyProcessedContract.getId(), toAdd);
									
									connector.insert(toBeConnected);
							}
						}
					} else {
						
						contractor.insert(contractToBeProcessed);
						
						contractToBeProcessed.setId(contractor.getRowCount());

						List<Integer> optionsToAdd = ui.getOptionsIdFromUiTable();
						if (optionsToAdd != null) {
							for (Integer toAdd : optionsToAdd) {
	
								toBeConnected = new ContractOption(0, contractor.getRowCount(), toAdd);
								
								connector.insert(toBeConnected);
								
							}
						}
											
						ui.addContractToDBTable(contractToBeProcessed);
						
					}
				 

			}
			/*
			 * END OF Action for Save menu item
			 */

			/*
			 * Actions for AddOption menu item in main contract window
			 */

			if (arg.getSource() == ui.getAddingOptionMenuItem()) {

				adding = new AddingOptionsWindow(ui);
				adding.addActionsToButtons(new AddingOptionsWindowActions());
				adding.setLocationRelativeTo(ui);
				adding.setVisible(true);

			}

			/*
			 * END OF Action for AddOption menu item
			 */

			/*
			 * New Contract Menu Item
			 */
			if (arg.getSource() == ui.getNewContractMenuItem()) {

				currentlyProcessedContract = null;
				ui.clearAllForNewContract();

			}
			
			if (arg.getSource() == ui.getEditOptionMenuItem()) {

				editing = new EditingOptionsWindow(ui);
				
				List<Option> options = optionator.selectAll();
				for(Option opt : options){
					editing.addOptionToTable(opt);
				}
				
				editing.addMouseListenerToTable(new EditingOptionsWindowActions());
				editing.addActionsForButtons(new EditingOptionsWindowActions());
				editing.setLocationRelativeTo(ui);
				editing.setVisible(true);

			}
		}
	}

	/*
	 * Class contains actions for AddingOptionsWindow
	 */

	class AddingOptionsWindowActions implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg) {

			/*
			 * Action for cancel button
			 */

			if (arg.getSource() == adding.getCancelButton()) {
				
				adding.dispatchEvent(new WindowEvent(adding, WindowEvent.WINDOW_CLOSING));

			}

			/*
			 * END OF Action for cancel button
			 */

			/*
			 * Action for confirming new option
			 */

			if (arg.getSource() == adding.getAddButton()) {

				AddingActions();

			}
			/*
			 * END OF Action for confirming new option
			 */

		}
		
		private void AddingActions(){
			
			try{
			Option optionToInsert = helpingService.getOptionFromUI(adding);

			optionator.insert(optionToInsert);
			
			adding.dispatchEvent(new WindowEvent(adding, WindowEvent.WINDOW_CLOSING));
			ui.setEnabled(true);
			ui.toFront();
			} catch (WrongDataException|ParseException e){
				return;
			}
			
		}

	}

	class SelectingOptionsWindowActions implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg) {

			/*
			 * Action for cancel button
			 */

			if (arg.getSource() == selecting.getCancelButton()) {
				
				selecting.dispatchEvent(new WindowEvent(selecting, WindowEvent.WINDOW_CLOSING));
				ui.setEnabled(true);
				ui.toFront();
			}

			/*
			 * END OF Action for cancel button
			 */

			if (arg.getSource() == selecting.getAddOptionToContractButton()) {
				if (selecting.getSelectedOptionId() < 0) {
					helpingService.showWarning("You didn't select any option.");
					return;
				} else {
					
					int selectedId = selecting.getSelectedOptionId();
					Option toAdd = optionator.selectById(selectedId);
					List<Integer> Ids = ui.getOptionsIdFromUiTable();
					
					for (Integer Id : Ids) {
						if (Id == toAdd.getId()) {
							helpingService.showWarning("You didn't select any option.");
							return;
						}
					}

					ui.addOptionToDBTable(toAdd);
					ui.updateSumarizedContractPrice();
					selecting.dispatchEvent(new WindowEvent(selecting, WindowEvent.WINDOW_CLOSING));
					ui.setEnabled(true);
					ui.toFront();
				}

			}

		}

	}
	
	class EditingOptionsWindowActions implements ActionListener, MouseListener {

		@Override
		public void actionPerformed(ActionEvent arg) {
				
				if(arg.getSource() == editing.getSaveButton()){
					
					try{
						Option toEdit = helpingService.getOptionFromUI(editing);
						
						optionator.update(toEdit);
						
						editing.clearOptionsDBTable();
						
						List<Option> options = optionator.selectAll();
						for(Option opt : options){
							editing.addOptionToTable(opt);
						}
					} catch (WrongDataException|ParseException e){
						return;
					}
					
				}
				
				if(arg.getSource() == editing.getCancelButton()){
					
					editing.dispatchEvent(new WindowEvent(editing, WindowEvent.WINDOW_CLOSING));
					ui.setEnabled(true);
					ui.toFront();
					
				}
				
				if(arg.getSource() == editing.getDeleteButton()){
					
					int confirmation = helpingService.askQuestion("Do you want to delete this option?");
					if (!(confirmation == JOptionPane.YES_OPTION))
						return;
					
					optionator.deleteById(editing.getSelectedOptionId());
					editing.deleteSelectedOption();
					
				}
			
		}	
		
		@Override
		public void mouseClicked(MouseEvent e) {
			
			if (e.getSource() == editing.getTable()) {
			    
				curentlyProcessedOption = optionator.selectById(editing.getSelectedOptionId());
				helpingService.passOptionToUI(editing, curentlyProcessedOption);
				
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		

	}

}
