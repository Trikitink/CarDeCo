package logic;

import java.text.ParseException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.alee.laf.WebLookAndFeel;

import gui.MainContractWindow;

public class MainApp {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(WebLookAndFeel.class.getCanonicalName());
		} catch (ClassNotFoundException e) {
			System.err.println("Class not found, problem with LaF");
			e.printStackTrace();
		} catch (InstantiationException e) {
			System.err.println("Cannot initialize, problem with LaF");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.err.println("Cannot access, problem with LaF");
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			System.err.println("Not supported, problem with LaF");
			e.printStackTrace();
		}

		MainContractWindow ui;
		try {
			ui = new MainContractWindow();

			ModelViewController controller = new ModelViewController(ui);

		} catch (ParseException e) {
			System.err.println("Cannot parse ui");
			e.printStackTrace();
		}

	}

}
