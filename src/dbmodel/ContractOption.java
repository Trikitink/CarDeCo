package dbmodel;

public class ContractOption extends BasicDatabaseObject{

	private int id;
	private int contract_id;
	private int option_id;

	public int getContract_id() {
		return contract_id;
	}

	public void setContract_id(int contract_id) {
		this.contract_id = contract_id;
	}

	public int getOption_id() {
		return option_id;
	}

	public void setOption_id(int option_id) {
		this.option_id = option_id;
	}

	public ContractOption(int id, int contract_id, int option_id) {

		super(id);
		this.contract_id = contract_id;
		this.option_id = option_id;

	}

	public String toString() {
		return "[" + id + "]" + " " + contract_id + " " + option_id;
	}

}
