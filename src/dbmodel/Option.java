package dbmodel;

public class Option extends BasicDatabaseObject{
	
	public enum OptionType{
		BY_KM,
		BY_MONTH,
		FIXED
	};
	
	private OptionType optionType;

	private String name;
	private String description;
	private int price;
	private boolean active;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public OptionType getOptionType(){
		return optionType;
	}
	
	public void setOptionType(OptionType optionType){
		this.optionType = optionType;
	}
	
	
	public Option(int id, String name, String description, int price, OptionType optionType, boolean active){
		
		super(id);

		this.name = name;
		this.description = description;
		this.price = price;
		this.optionType = optionType;
		this.active = active;
		
	};
	
	@Override
	public String toString(){
		return "["+id+"]"+name+" "+description+" "+price+" "+optionType+" "+active;
	}

}
