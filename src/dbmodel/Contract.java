package dbmodel;

public class Contract extends BasicDatabaseObject{
	
	private String brand;
	private String model;
	
	private int price;
	private int prodYear;
	private int carMileage;
	
	private int contractPrice;
	private int contractMileage;
	
	private String signingDate;
	private String contractStartDate;
	private String contractEndDate;


	
	public int getPrice() {
		return price;
	}



	public void setPrice(int price) {
		this.price = price;
	}



	public int getProdYear() {
		return prodYear;
	}



	public void setProdYear(int prodYear) {
		this.prodYear = prodYear;
	}



	public int getMileage() {
		return carMileage;
	}



	public void setMileage(int mileage) {
		this.carMileage = mileage;
	}



	public String getBrand() {
		return brand;
	}



	public void setBrand(String brand) {
		this.brand = brand;
	}



	public String getModel() {
		return model;
	}



	public void setModel(String model) {
		this.model = model;
	}



	public int getContractMileage() {
		return contractMileage;
	}



	public void setContractMileage(int contractMileage) {
		this.contractMileage = contractMileage;
	}



	public int getContractPrice() {
		return contractPrice;
	}



	public void setContractPrice(int contractPrice) {
		this.contractPrice = contractPrice;
	}



	public String getSigningDate() {
		return signingDate;
	}



	public void setSigningDate(String signingDate) {
		this.signingDate = signingDate;
	}



	public String getContractStartDate() {
		return contractStartDate;
	}



	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}



	public String getContractEndDate() {
		return contractEndDate;
	}



	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}



	public Contract(int id, String brand, String model, int price, int prodYear, int mileage, int contractMileage,
			int contractPrice, String signingDate, String contractStartDate, String contractEndDate) {
		super(id);
		this.brand = brand;
		this.model = model;
		this.price = price;
		this.prodYear = prodYear;
		this.carMileage = mileage;
		this.contractPrice = contractPrice;
		this.contractMileage = contractMileage;
		this.signingDate = signingDate;
		this.contractStartDate = contractStartDate;
		this.contractEndDate = contractEndDate;
	}



	@Override
	public String toString(){
		return "["+id+"]"+brand+" "+model+" "+prodYear+" "+contractStartDate+" "+contractEndDate;
	}
	
}
