package dbmodel;

public abstract class BasicDatabaseObject {
	protected int id;
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	BasicDatabaseObject(int id){
		this.id = id;
	}
}
